// implementaciones de referencia:
// fs-chunk-store - https://github.com/feross/fs-chunk-store/blob/master/test.js
// immediate-chunk-store - https://github.com/feross/immediate-chunk-store/blob/master/test/basic.js
const abstractTests = require('abstract-chunk-store/tests')
const parallel = require('run-parallel')
const Store = require('./')
const test = require('tape')

// check abstract-chunk-store api implementation
abstractTests(test, Store)

// com.flixxo.lib.torrent/src/middleware/flixxwire/wtflixx.js
const FLX_SIZEOF_PART = 16384; // 16k
const chunkLength = FLX_SIZEOF_PART

function makePutMaxTest(exceed){
  return function (t) {
    const maxCapacity = 10 * 1024 * 1024 // 10MB
    const store = new Store(chunkLength, {maxCapacity})
    const totalChunks = store.totalChunks

    // https://nodejs.dev/learn/nodejs-buffers
    const data = Buffer.alloc(chunkLength)
    data.fill('1')

    function endTask(){
      store.destroy(function (err) {
        t.error(err, 'No error')
        t.end()
      })
    }

    function makePutTask (chunkI) {
      return function (cb) {
        store.put(chunkI, data, cb)
      }
    }

    let tasks = []
    for (let chunkI = 0; chunkI <= totalChunks; ++chunkI) {
      tasks.push(makePutTask(chunkI))
    }

    parallel(tasks, function (err) {
      t.error(err, 'No error')
      if (!exceed)
        endTask()
      else {
        const exceedTask = makePutTask(totalChunks + 1)
        exceedTask(function (err) {
          if (err)
            t.match(err.toString(), /Exceeded max chunks index/, 'Exceed error detected')
          else
            t.error('Exceed error not detected')
          endTask()
        })
      }
    })
  }
}
// tape tests - https://www.npmjs.com/package/tape
test('flixxo: put max no exceed', makePutMaxTest(false));
test('flixxo: put max exceed', makePutMaxTest(true));
