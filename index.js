const queueMicrotask = require('queue-microtask')
const ImmediateChunkStore = require('immediate-chunk-store')
const FSChunkStore = require('fs-chunk-store')

// imprementaciones de referencia:
// immediate-chunk-store - https://github.com/feross/immediate-chunk-store/blob/master/index.js
// fs-chunk-store - https://github.com/feross/fs-chunk-store/blob/master/index.js
/**
 * Represents a book.
 * @constructor
 * @param {number} chunkLength - Size of chunks in bytes
 * @param {Object} [opts] - Options
 * @param {number} [opts.maxCapacity] - Maximum total size of store in bytes
 */
class Store {
  constructor (chunkLength, opts) {
    this.chunkLength = Number(chunkLength)
    if (!this.chunkLength) throw new Error('First argument must be a chunk length')

    if (!opts) opts = {}

    this.maxCapacity = Number(opts.maxCapacity)
    if (!this.maxCapacity){
      this.maxCapacity = Infinity
      this.totalChunks = Infinity
    }else{
      this.totalChunks = Math.ceil(this.maxCapacity / this.chunkLength)
    }

    // ImmediateChunkStore constructor recibe un store
    // FSChunkStore constructor recibe chunkLength y
    // opts = {path, length} o {files: [{path, length}, ...]}
    this.store = new ImmediateChunkStore(new FSChunkStore(this.chunkLength, opts))
  }

  put (index, buf, cb) {
    if (index > this.totalChunks){
      return nextTick(cb, new Error('Exceeded max chunks index ' + this.totalChunks + ' ('+index+')'))
    }

    return this.store.put(index, buf, cb)
  }

  get (index, opts, cb) {
    return this.store.get(index, opts, cb)
  }

  close (cb) {
    return this.store.close(cb)
  }

  destroy (cb) {
    return this.store.destroy(cb)
  }
}

function nextTick (cb, err, val) {
  queueMicrotask(function () {
    if (cb) cb(err, val)
  })
}

module.exports = Store
