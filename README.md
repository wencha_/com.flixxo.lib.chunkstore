https://github.com/mafintosh/abstract-chunk-store

## Instalación

Clonar repo e instalar dependencias con comando:

`yarn`

## Tests
Para realizar los testeos hacer:

`yarn test`

Para ver el detalle del testeo hacer:

`yarn test-debug`

## Descripción
La operatoria básica de un chunk store es que guarda datos por índice (0, 1, 2, ...)
donde cada índice guarda datos de `chunkLength` bytes de largo, ni más ni menos.

Se podría pensar que los índice son como cajones, y los datos dentro de cada índice
o cajón solo pueden ser tan anchos como el mueble, que tiene `chunkLength` de ancho.
La única salvedad es que en este caso los cajones siempre tienen que estar llenos
(aunque sea de ceros!).

Sin embargo, como se ven en las opciones del método `get` se pueden traer secciones
de un mismo chunk y no el chunk entero (definido por `offset` y `len`).

## API de abstract-chunk-store
_Los valores entre [corchetes] quiere decir que son opcionales_
* `constructor(chunkLength)`
* `.chunkLength`
* `.put(i, data, [cb(err)])`
* `.get(i, [opts], cb(err, data))`
  * `opts.offset` opcional, mayor o igual que 0
  * `opts.length` opcional, menor o igual que `chunkLength`
* `.close([cb(err)])`
* `.destroy([cb(err)])`

Esto se puede ver en los
[testeos oficiales](https://github.com/mafintosh/abstract-chunk-store/blob/master/tests/index.js)

## Implementaciones de referencia
* [fs-chunk-store](https://www.npmjs.com/package/fs-chunk-store) almacena los chunks en un
archivo o varios en disco. Usa
[random-access-file](https://www.npmjs.com/package/random-access-file)
([fuente](https://github.com/feross/fs-chunk-store/blob/master/index.js#L69)).

* [immediate-chunk-store](https://www.npmjs.com/package/immediate-chunk-store) disponibiliza
los chunks en memoria antes de que sean guardado en el verdadero store, y cuando efectivamente
se guardan en el store los borra de memoria y posteriormente los brinda directamente del store
([fuente](https://github.com/feross/immediate-chunk-store/blob/master/index.js)).

* [chunk-store-stream](https://www.npmjs.com/package/chunk-store-stream) expone dos objetos,
uno para leer y otro para escribir a streams.


### Esta implementación

La librería [webtorrent](https://www.npmjs.com/package/webtorrent) soporta chunk
stores, como se puede ver descripto en la API oficial de
[client.add](https://github.com/webtorrent/webtorrent/blob/master/docs/api.md#clientaddtorrentid-opts-function-ontorrent-torrent-)
.

Se llama al constructor con los siguientes parámetros (como se puede ver en el
[código fuente](https://github.com/webtorrent/webtorrent/blob/master/lib/torrent.js#L478)):


 `constructor(chunkLength, storeOpts)`

 donde `storeOpts` contendrá:

 * `torrent` un objeto con una propiedad `infoHash`
 * `name` el mismo `infoHash` anterior
 * `files` un array de objetos con las propiedades `path`, `length` y `offset`
 * `length` tamaño del total de archivos del torrent

Además como se ve en el
[código fuente](https://github.com/webtorrent/webtorrent/blob/master/lib/torrent.js#L1620)
el índice que se le pasa a `put` es el mismo que el `bitfield` y las `pieces`
del objeto `Torrent`.

---
`com.flixxo.lib.torrent/src/middleware/flixxwire/wtflixx.js`:

`const FLX_SIZEOF_PART = 16384; // 16k`
